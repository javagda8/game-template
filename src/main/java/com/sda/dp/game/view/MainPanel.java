package com.sda.dp.game.view;

import com.sda.dp.game.model.AbstractGameObject;
import com.sda.dp.game.model.GameHero;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class MainPanel extends JPanel{
    private final Color BACKGROUND_COLOR = Color.GREEN;
    private final int width;
    private final int height;

    private List<AbstractGameObject> objectList;

    private GameHero hero;

    public MainPanel(int width, int height) {
        super();
        this.width = width;
        this.height = height;

        // create stuff
        objectList = new ArrayList<>();

        // set stuff
        setBorder(BorderFactory.createLineBorder(Color.black, 2));
        setLayout(null);
    }

    public void addCharacterIntoGame(AbstractGameObject gameObject){
        objectList.add(gameObject);
    }

    public void setGameHero(GameHero hero){
        this.hero = hero;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(BACKGROUND_COLOR);
        g2d.fillRect(0, 0, width, height);

        g2d.setColor(Color.white);
        AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
        objectsToPaint = objectList.toArray(objectsToPaint);

        for (AbstractGameObject objectToPaint : objectsToPaint) {
            objectToPaint.paint(g2d);
        }

        hero.paint(g2d);
    }

    public void move(double move){
        if(move > 0) {
            AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
            for (AbstractGameObject objectToPaint : objectsToPaint) {
                if(objectToPaint != null) {
                    objectToPaint.move(move);
                }
            }

            hero.move(move);
        }
    }
}
