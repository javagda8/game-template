package com.sda.dp.game.model;

import java.awt.*;

public class CircleObject extends AbstractGameObject{

    public CircleObject(int x, int y) {
        super(new Point(x, y));
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        // zapamiętuje kolor jaki był ustawiony
        Color currentColor = g2d.getColor();

        // set color
        g2d.setColor(Color.RED);
        // paint something
        g2d.fillOval(position.x, position.y, 50, 50);

        g2d.drawRect(position.x - 20, position.y - 20, 90, 90);

        // ustawiam zapamiętany wcześniej kolor
        g2d.setColor(currentColor);
    }
}
