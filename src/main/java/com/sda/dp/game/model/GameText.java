package com.sda.dp.game.model;

import java.awt.*;

public class GameText extends AbstractGameObject {

    private String text;

    public GameText(int x, int y, String text) {
        super(new Point(x, y));
        this.text = text;
    }

    @Override
    public void paint(Graphics2D g2d) {
        Color currentColor = g2d.getColor();

        // set color
        g2d.setColor(Color.RED);
        // paint something
        g2d.drawString(text, this.position.x, this.position.y );

        // set last color
        g2d.setColor(currentColor);
    }
}
